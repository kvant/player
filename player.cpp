#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <SLib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <arpa/inet.h>
#include <netinet/sctp.h>

#define GT_SIZE		1024
#define MAX_TRACKS	40
#define POINTS_INC	20
#define MAX_CONNECTIONS	20
#define DEF_SEND_INT	10
#define MAX_EVENTS	10

#define log(...) fprintf(stderr, __VA_ARGS__)

using namespace slib;
using namespace geo;
using namespace proto;
using namespace timesupport;


static FILE *ff;
static struct timespec start_time;
static unsigned numtracks;
static unsigned numconns;
static unsigned max_time;
static int	udp_sock;
static int	sctp_sock;
static double GaussTable[GT_SIZE];

pthread_mutex_t conn_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t d_thrd;

enum ConType {
	CT_SCTP,
	CT_UDP,
};

struct params_t {
	bool		debug;
	bool		daemon;
	unsigned	sleep;		/* start*/
	unsigned	interval; 	/* point send interval */
	unsigned	margin; 	/* point timestamp margin */
	unsigned	offset; 	/* offset */
	unsigned	finish; 	/* offset */
	double		Ra;
	double		Re;
	double		Rd;
} params = {0};

struct Point {
	int		valid;
	unsigned	time;
	double		X, Y, Z;
	double		A, B, D;
	char		snd_data[TR_POINT_SIZE + 10];
};

struct Track {
	unsigned	id;
	unsigned	start;
	unsigned	finish;
	unsigned	npoints;
	unsigned	current;
	unsigned	lasttime;
	struct Point	*points;
};

struct Connection {
 	int			sock;
	bool			server;
	struct sockaddr_in	addr;
	ConType 		type;
	int			(* send)(struct Connection *con, void *data, size_t sz);
};

struct Connection Conns[MAX_CONNECTIONS];

static struct Track Tracks[MAX_TRACKS];

void PrepareGauss (void)
{
	int    cur_el = 0;
	double step = 1.0 / double(GT_SIZE);
	double int_step = step / 1e3;
	double cur_step = 0;
	double int_sum = 0;
	double cur_param = -4.0;
	double cur_val = exp(-8.0) / SQRT_2PI;
	double old_val = cur_val;
	do {
		cur_step = step * cur_el * SQRT_2PI;
		while (int_sum < cur_step) {
			cur_param += int_step;
			cur_val = exp(-(cur_param * cur_param / 2.0));
			int_sum += (cur_val + old_val) * int_step / 2.0;
			old_val = cur_val;
		}
		GaussTable[cur_el] = cur_param;
	} while (++cur_el != GT_SIZE);

	log("Gauss table from %d items has been computed\n", GT_SIZE);
}

double GetGaussTab(double m, double sig)
{
	unsigned item = static_cast<unsigned>(((double)random() /
					(double)RAND_MAX) * GT_SIZE);
	return (m + GaussTable[item] * sig);
}


static void usage(void)
{
	log("Help:\n");
}

static void inc_time(struct timespec &tm, unsigned ms)
{
	struct timespec ta = { ms / 100, (ms % 100) * 10000000};
	tm += ta;
}

static void dec_time(struct timespec &tm, unsigned ms)
{
	struct timespec ta = { ms / 100, (ms % 100) * 10000000};
	tm -= ta;
}

static int open_file(char const *path)
{
	if(access(path, O_RDONLY))
		goto err;

	if(!(ff = fopen(path, "r")))
		goto err;

	return 0;
err:
	log("can't open file with name: %s", path);
	return -1;
}

static void *delivery_thread(void *arg)
{
	unsigned nselected;
	unsigned ntime = params.offset;
	unsigned numbers[MAX_TRACKS];
	struct timespec next_time;
	struct timespec cur_time;
	struct timespec rem;
	struct timepascal tpas;	

	if (params.sleep)
		sleep(params.sleep);

	clock_gettime(CLOCK_REALTIME, &start_time);

	if (ntime)
		dec_time(start_time, ntime);

	while(1) {
		nselected = 0;
		/* search for next points */
		do {
			ntime += params.interval;
			for(unsigned i=0; i < numtracks; i++) {
				if (Tracks[i].finish >= ntime)
					for (unsigned j = Tracks[i].current + 1; j < Tracks[i].npoints; j++)
						if (Tracks[i].points[j].time <= ntime &&
								((ntime - Tracks[i].points[j].time) <= params.margin)) {
							Tracks[i].current = j;
							numbers[nselected] = i;
							nselected++;
							next_time = start_time;
							inc_time(next_time, Tracks[i].points[j].time);
							tspec2tpascal(next_time, tpas);
							*((uint32_t *)&Tracks[i].points[Tracks[i].current].snd_data[10]) = tpas.tp_usec;
						}
			}
			log("ntime = %u\n", ntime);
		} while(!nselected && ntime < max_time);

		if (!nselected || ntime >= max_time) {
			fprintf(stderr, "Nothing to send more. Seems simulation is over\n");
			exit(0);
		}

		/* wait for next time */
		next_time = start_time;
		inc_time(next_time, ntime);
		clock_gettime(CLOCK_REALTIME, &cur_time);
#if 0
		log("next_time = %u:%u\n", next_time.tv_sec, next_time.tv_nsec);
		log("cur_time  = %u:%u\n", cur_time.tv_sec, cur_time.tv_nsec);
#endif
		if (cur_time > next_time) {
			fprintf(stderr, "Warning: Time overlaping. Force sleep to 0\n");
		} else {
			next_time -= cur_time;
#if 0
			log("need sleep = %u:%u\n", next_time.tv_sec, next_time.tv_nsec);
#endif
			nanosleep(&next_time, &rem);
		}

		/* send selected points */
		for (unsigned i = 0; i < nselected; i++) {
			pthread_mutex_lock(&conn_mutex);
#if 0
			log("Sending track %d time: %u\n", Tracks[numbers[i]].id, ntime);
#endif
			for (unsigned j = 0; j < numconns; j++) {
				if (Conns[j].server)
					continue;
				if (Conns[j].send(&Conns[j], Tracks[numbers[i]].points[Tracks[numbers[i]].current].snd_data,
							sizeof(Tracks[numbers[i]].points[Tracks[numbers[i]].current].snd_data))) {
					log("Error during send to connection: %d\n", Conns[j].sock);
				}
			}
			pthread_mutex_unlock(&conn_mutex);
		}
	}

	return NULL;
}

static void data_delivery(void)
{
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&d_thrd, &attr, delivery_thread, NULL);
}



static int parse_file(int flags)
{
	char *buff = (char *)malloc(512);
	char stub[100];
	size_t n, mtsz;
	float x, y, z;
	unsigned number, time, i, sz;
	struct msgTrackPoint mt_point = {0};

	memset(Tracks, 0x00, sizeof(Tracks));

	while(getline(&buff, &n, ff) != -1) {
		number = sizeof(buff);
		sscanf(buff, "%u\t%s\t%s\t%s\t%s\t%s\t%g\t%g\t%g\t%s\t%s\t%s\t%s\t%s\t%u\t%s",
				&number, stub, stub, stub, stub, stub, &x, &y, &z, stub, stub, stub, stub, stub, &time, stub);
		for(i = 0; Tracks[i].id ^ number && Tracks[i].id; i++);

		if (!Tracks[i].id) {
			Tracks[i].id = number;
			Tracks[i].points = (struct Point *)malloc(sizeof(struct Point) *
										POINTS_INC);
			numtracks++;
		}

		Tracks[i].points[Tracks[i].npoints].X = x;
		Tracks[i].points[Tracks[i].npoints].Y = y;
		Tracks[i].points[Tracks[i].npoints].Z = z;
		Tracks[i].points[Tracks[i].npoints].time = time;
		if (Tracks[i].start > time || !Tracks[i].npoints)
			Tracks[i].start = time;

		if (Tracks[i].finish < time) {
			Tracks[i].finish = time;
			if (time > max_time)
				max_time = time;
		}

		/* set some field in track point struct */
		mt_point.CoordsDec.X = x;
		mt_point.CoordsDec.Y = y;
		mt_point.CoordsDec.Z = z;
		mt_point.ObjSrcNumber = number;
		mt_point.ObjModNumber = number;
		Dec3D2Sphere(mt_point.CoordsDec, mt_point.CoordsSph);
		mt_point.CoordsSph.Azimuth = RAD_TO_DEG(mt_point.CoordsSph.Azimuth);
		mt_point.CoordsSph.Elevation = RAD_TO_DEG(mt_point.CoordsSph.Elevation);

		/* apply gauss deviation */
		mt_point.CoordsSph.Azimuth = GetGaussTab(mt_point.CoordsSph.Azimuth, params.Ra);
		mt_point.CoordsSph.Elevation = GetGaussTab(mt_point.CoordsSph.Elevation, params.Re);
		mt_point.CoordsSph.Distance = GetGaussTab(mt_point.CoordsSph.Distance, params.Rd);

		convTrackPoint2Data(mt_point, &Tracks[i].points[Tracks[i].npoints].snd_data[10], mtsz);
		Tracks[i].points[Tracks[i].npoints].snd_data[8] = 0;
		Tracks[i].points[Tracks[i].npoints].snd_data[9] = 1;

		Tracks[i].npoints++;

		if (!(Tracks[i].npoints % POINTS_INC)) {
			sz = sizeof(struct Point) * (Tracks[i].npoints + POINTS_INC);
			Tracks[i].points = (struct Point *)realloc(Tracks[i].points, sz);
		}
	}

	for(i = 0; i < numtracks; i++) {
		log("Track[%u] has %u points. Start=%u Finish=%u\n", Tracks[i].id, Tracks[i].npoints,
								Tracks[i].start, Tracks[i].finish);
	}
	if (params.finish)
		max_time = params.finish;

	log("Maximum time is %d\n", max_time);
	return 0;
}

static int send_sctp(struct Connection *con, void *data, size_t sz)
{
	int ret = send(con->sock, data, sz, 0);
	return (!(ret == (int)sz));
}

static int send_udp(struct Connection *con, void *data, size_t sz)
{
	int ret = sendto(con->sock, data, sz, 0, (const struct sockaddr *)&con->addr, sizeof(struct sockaddr_in));
	return (!(ret == (int)sz));
}

void updateParams()
{
	if (!params.interval) {
		log("Seting send interval to default %d msec\n", DEF_SEND_INT);
		params.interval = DEF_SEND_INT;
	}
}

int addSCTPClient(int sock)
{
	struct sockaddr addr;
	int ndelay = 1;
	socklen_t len = sizeof(addr);

	int nsock = accept(sock, &addr,&len);

	if (nsock == -1) {
		log("Error SCTP client accepting\n");
		goto exit;
	}

	setsockopt(nsock, SOL_SCTP, SCTP_NODELAY, &ndelay, sizeof(ndelay));

	Conns[numconns].sock = nsock;
	Conns[numconns].addr = *((struct sockaddr_in*)&addr);
	Conns[numconns].send = send_sctp;
	Conns[numconns].type = CT_SCTP;
	Conns[numconns].server = false;
	numconns++;

	log("Client accepted\n");
exit:
	return nsock;
}

void delSCTPSclient(int sock)
{
	char buff[512];


	if (recv(sock, buff, sizeof(buff), 0) > 0) {
		log("Fake read from client\n");
		return;
	}

	for (unsigned i=0; i < numconns; i++) {
		if(Conns[i].sock == sock) {
			close(sock);
			numconns--;
			for ( ; i < numconns; i++)
				Conns[i] = Conns[i + 1];
			break;
		}
	}
	log("Client delted \n");
}

void addServerEP(char *arg)
{
	int sock;
	int ndelay = 1;
	char *column = strchr(arg, ':');
	struct sockaddr_in addr = {0};

	if (!column) {
		log("SCTP EP: No column found. Assuming just port.\n");
		addr.sin_family = AF_INET;
		addr.sin_port = htons((unsigned short)atoi(optarg));
	} else {
		column[0] = '\0';
		addr.sin_family = AF_INET;
		addr.sin_port = htons((unsigned short)atoi(&column[1]));
		addr.sin_addr.s_addr = inet_addr(optarg);
	}

	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP)) == -1) {
		log("Can't create SCTP server socket\n");
		exit(1);
	}

	setsockopt(sock, SOL_SCTP, SCTP_NODELAY, &ndelay, sizeof(ndelay));
	if (bind(sock, (struct sockaddr*)&addr, sizeof(addr))) {
		log("Can't bind socket to addr\n");
		exit(1);
	}

	if (listen(sock, 5)) {
		log("Can't switch to listen mode \n");
		exit(1);
	}

	sctp_sock = sock;
	Conns[numconns].sock = sock;
	Conns[numconns].addr = addr;
	Conns[numconns].send = NULL;
	Conns[numconns].type = CT_SCTP;
	Conns[numconns].server = true;
	numconns++;
}

void initUDPServer(void)
{
	if ((udp_sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		log("Can't create UDP server socket\n");
		exit(1);
	}
}

void addUDPClient(char *arg)
{
	char *column = strchr(arg, ':');
	struct sockaddr_in addr = {0};

	if (!udp_sock)
		initUDPServer();

	if (!column) {
		log("UDP EP: No column found. Assuming localhost.\n");
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = inet_addr("127.0.0.1");
		addr.sin_port = htons((unsigned short)atoi(optarg));
	} else {
		column[0] = '\0';
		addr.sin_family = AF_INET;
		addr.sin_port = htons((unsigned short)atoi(&column[1]));
		addr.sin_addr.s_addr = inet_addr(optarg);
	}

	Conns[numconns].sock = udp_sock;
	Conns[numconns].addr = addr;
	Conns[numconns].send = send_udp;
	Conns[numconns].type = CT_UDP;
	Conns[numconns].server = false;
	numconns++;

	log("UDP client has been added: \n");
}

static int connection_processing(void)
{
	struct epoll_event ev, events[MAX_EVENTS];
	int conn_sock, nfds, epollfd;

	/* Set up listening socket, 'listen_sock' (socket(),
	 bind(), listen()) */

	epollfd = epoll_create(10);
	if (epollfd == -1) {
		perror("epoll_create");
		exit(EXIT_FAILURE);
	}

	if (!(sctp_sock && udp_sock)) {
		if (udp_sock) {
			log("Warning: No SCTP server. Only UDP\n");
			goto udp_mode;
		}
		if (!sctp_sock) {
			log("Error: any server sockets\n");
			exit(1);
		}
		log("Warning: Only SCTP mode\n");
	}

	ev.events = EPOLLIN;
	ev.data.fd = sctp_sock;
	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, sctp_sock, &ev) == -1) {
		perror("epoll_ctl: listen_sock");
		exit(EXIT_FAILURE);
	}

	for (;;) {
		nfds = epoll_wait(epollfd, events, MAX_EVENTS, -1);
		if (nfds == -1) {
			perror("epoll_pwait");
			exit(EXIT_FAILURE);
		}

		pthread_mutex_lock(&conn_mutex);
		for (int n = 0; n < nfds; ++n) {
			if (events[n].data.fd == sctp_sock) {
				if ((conn_sock = addSCTPClient(sctp_sock)) == -1) {
					perror("accept");
					exit(EXIT_FAILURE);
				}
				ev.events = EPOLLIN | EPOLLET;
				ev.data.fd = conn_sock;
				if (epoll_ctl(epollfd, EPOLL_CTL_ADD, conn_sock,
									&ev) == -1) {
					perror("epoll_ctl: conn_sock");
					exit(EXIT_FAILURE);
				}
			} else {
				delSCTPSclient(events[n].data.fd);
			}
		}
		pthread_mutex_unlock(&conn_mutex);

	}
	return 0;

udp_mode:
	while(1)
		sleep(100);
}

void parseRMS(char *arg)
{
	switch (arg[0])
	{
	case 'a':
	case 'A':
		params.Ra = atof(arg + 1);
		break;
	case 'e':
	case 'E':
		params.Re = atof(arg + 1);
		break;
	case 'd':
	case 'D':
		params.Rd = atof(arg + 1);
		break;
	default:
		log("Incorrect RMS value\n");
		exit(1);
	}
}

int main(int argc, char *argv[])
{
	int opt;

	PrepareGauss();

	while((opt = getopt(argc, argv, "f:dw:i:m:o:u:s:t:r:")) != -1) {
		switch(opt) {
		case 'f': /* file name */
			if (open_file(optarg))
				abort();
			break;
		case 'd': /* debug mode */
			params.debug = true;
			break;
		case 'w': /* start delay */
			params.sleep = atoi(optarg);
			break;
		case 'e': /* UDP send addr */
			break;
		case 'i': /* */
			params.interval = atoi(optarg);
			break;
		case 's': /* server endpoint */
			addServerEP(optarg);
			break;
		case 'u': /* UDP client */
			addUDPClient(optarg);
			break;
		case 'm': /* margin */
			params.margin = atoi(optarg);
			break;
		case 'o': /* offset */
			params.offset = atoi(optarg);
			break;
		case 't': /* finish */
			params.finish = atoi(optarg);
			break;
		case 'r': /* RMS */
			parseRMS(optarg);
			break;

		default:
			log("Unknown arg \n");
			usage();
			abort();
		}
	}

	updateParams();

	if (!ff) {
		log("File not opened\n");
		abort();
	}
	parse_file(0);
	fclose(ff);


	data_delivery();
	connection_processing();


	return 0;
}

