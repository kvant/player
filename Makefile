
OBJS := player.o
CFLAGS := -g -Wall -pthread -O0 -I/home/ryazantsev/Develop/KVANT/2012/install/slib/include/slib
CXXFLAGS := ${CFLAGS}
LDFLAGS := -L/home/ryazantsev/Develop/KVANT/2012/install/slib/lib -static

all: ${OBJS}
	@g++  ${OBJS} -o player -lrt -ls ${LDFLAGS} ${CFLAGS}
	@rm ${OBJS}
clean:
	rm -rf ${OBJS} player
